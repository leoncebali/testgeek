package com.example;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class MyGenerator {

    private static final int SCHEMA_VERSION = 1;

    public static void main(String args[]) throws Exception {
        Schema schema = new Schema(SCHEMA_VERSION, "tg.geeknation.rajkat.models");

        schema.setDefaultJavaPackageDao("tg.geeknation.rajkat.models.dao");
        schema.setDefaultJavaPackageTest("tg.geeknation.rajkat.models.test");

        addEntity(schema);

        new DaoGenerator().generateAll(schema, "app/src/main/java");
    }

    private static void addEntity(Schema schema) {

        /*Création de l'entité TypeCategorie*/
        Entity profil = schema.addEntity("Profil");
        profil.implementsSerializable();
        profil.addIdProperty();
        profil.addStringProperty("nom");
        profil.addStringProperty("competence");
        profil.addIntProperty("photo");
    }
}
